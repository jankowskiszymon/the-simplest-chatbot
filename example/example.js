const theSimplestChatbot = require('../../chatbot-framework')
    
    //define the training data for the chatbot 
    const data = {
    settings: {
        precision: 0.7, // {0 ... 1}
        defaultAnswer: ["I'm sorry", "I can't uderstand you :(", "I don't know"]
    },
    trainingPhrases: {
        nameOfIntent: {
            usersays: [
                'Training usersays'
            ],
            answer: ['Answer to these questions']
        },
        hello: {
            usersays: [
                'greetings',
                'Hello',
                'Hi',
                'welcome',
                'bonjour',
                'buenas noches',
                'Dzień dobry',
                'buenos dias',
                'good day'
            ],
            answer: ['Awww! Hello world!']
        },
        goodbye: {
            usersays: [
                'Bye',
                'goodbye',
                'do widzenia',
                'see you'
            ],
            answer: ['Goodbye my friend','see you!']
        },
        and_so_on: {
            usersays: [
                'what is the answer to all questions',
                'Tell me something interesting'
            ],
            answer: ['Banging your head against a wall for one hour burns 150 calories.', 'Pteronophobia is the fear of being tickled by feathers.']
        }
    }
}

const result = theSimplestChatbot({
    usersay: 'Tell me something interestin', 
    data: data
})

console.log(result) 
// {
//     answer: 'Banging your head against a wall for one hour burns 150 calories.',
//     intent: 'and_so_on',
//     distance: 0.9933002481389578
// }