const jaroWinkler = require('./jaro-winkler.js')

test('Test jaro-winkler, string ===string', () => {
    expect(jaroWinkler('abc', 'abc').toBe(1))
})

test('Test jaro-winkler, string ~~~ string', () => {
    expect(jaroWinkler("Hello world", "Helo Word").toBeGreaterThan(0.5))
})

test('Test jaro-winkler, string != string', () => {
    expect(jaroWinkler('abc', 'asdfsadfasdf').toBe(0))
})