const jaroWinkler = require('./utils/jaro-winkler')
const getMessageFromArray = require('./utils/getMessageFromArray')

const miniChatbot = ({
    usersay,
    data: {settings, trainingPhrases}
}) => {
    const keys = Object.keys(trainingPhrases)
    const {
        precision,
        defaultAnswer
    } = settings

    let max = 0
    let intent = 'default intent'
    let answer

    keys.forEach(key => {
        trainingPhrases[key].usersays.forEach(trainingSay => {
            const distance = jaroWinkler(usersay, trainingSay)
            if(distance > max) {
                max = distance
                intent = key
                answer = getMessageFromArray(trainingPhrases[key].answer)
            }
        })
    })

    return max > precision ? {
        usersay: usersay,
        answer: answer,
        intent: intent,
        distance: max
    } : {
        usersay: usersay,
        answer: getMessageFromArray(defaultAnswer),
        intent: intent,
        distance: max
    }
}

module.exports = miniChatbot;