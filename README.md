# The simplest chatbot

The simplest chatbot is the simplest chatbot framework. With this tool you can easly create your own simple chatbot.
# New Features!

  - Set Presision for Jaro-Winkler distance Algorithm 
  - Create an array with questions, and answers for these questions


### Installation

The Simplest Chatbot requires [Node.js](https://nodejs.org/) v4+ to run.

```sh
$ cd theSimplestChatbot
$ git clone https://gitlab.com/jankowskiszymon/the-simplest-chatbot.git .
$ rm -rf .git
```

### How to use it

```javascript
const theSimplestChatbot = require('../../chatbot-framework')
    
    //define the training data for the chatbot 
const data = {
    settings: {
        precision: 0.7, // {0 ... 1}
        defaultAnswer: ["I'm sorry", "I can't uderstand you :(", "I don't know"]
    },
    trainingPhrases: {
        nameOfIntent: {
            usersays: [
                'Training usersays'
            ],
            answer: ['Answer to these questions']
        },
        hello: {
            usersays: [
                'greetings',
                'Hello',
                'Hi',
                'welcome',
                'bonjour',
                'buenas noches',
                'Dzień dobry',
                'buenos dias',
                'good day'
            ],
            answer: ['Awww! Hello world!']
        },
        goodbye: {
            usersays: [
                'Bye',
                'goodbye',
                'do widzenia',
                'see you'
            ],
            answer: ['Goodbye my friend','see you!']
        },
        and_so_on: {
            usersays: [
                'what is the answer to all questions',
                'Tell me something interesting'
            ],
            answer: ['Banging your head against a wall for one hour burns 150 calories.', 'Pteronophobia is the fear of being tickled by feathers.']
        }
    }
}

const result = theSimplestChatbot({
    usersay: 'Tell me something interestin', //typo made on purpose 
    data: data
})

console.log(result) 
// {
//     answer: 'Banging your head against a wall for one hour burns 150 calories.',
//     intent: 'and_so_on',
//     distance: 0.9933002481389578
// }

```

Made by Szymon Jankowski with <3
